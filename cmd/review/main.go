package main

import (
	"context"
	"flag"
	"github.com/getsentry/sentry-go"
	"gitlab.services.mts.ru/performance-review/review"
	reviewv1 "gitlab.services.mts.ru/performance-review/review/internal/app/review-v1"
	reviewv1pb "gitlab.services.mts.ru/performance-review/talent-review-api/gen/go/review/v1"
	"log/slog"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.services.mts.ru/performance-review/review/internal/config"
	"gitlab.services.mts.ru/performance-review/review/internal/migration"
	"gitlab.services.mts.ru/performance-review/review/internal/storage"
	"gitlab.services.mts.ru/performance-review/review/internal/transport/http/handler"
	"gitlab.services.mts.ru/performance-review/review/internal/transport/interceptor"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/encoding/protojson"
)

const (
	_configPath = "./config/config.dev.yml"
)

const (
	_maxOpenConn     = 150
	_maxIdleConn     = 50
	_maxConnLifetime = time.Second * 10
)

func main() {
	var (
		ctx = context.Background()
	)

	var (
		configFile = flag.String("config", _configPath, "config file path")
	)
	flag.Parse()

	// Инициализация слушителя системных сигналов
	signalListener := make(chan os.Signal, 1)
	defer close(signalListener)

	signal.Notify(signalListener,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)

	// Инициализация логера
	opts := &slog.HandlerOptions{
		Level: slog.LevelDebug,
	}
	slog.SetDefault(slog.New(slog.NewJSONHandler(os.Stdout, opts)))

	// Инициализация конфига
	cfg, err := config.NewConfig(*configFile)
	if err != nil {
		slog.Error("init config", "error", err)
		return
	}

	// Инициализация Sentry
	err = sentry.Init(sentry.ClientOptions{
		Dsn:              cfg.Sentry.DSN,
		EnableTracing:    true,
		TracesSampleRate: 0.3,
	})
	if err != nil {
		slog.Error("init sentry", "error", err)
		return
	}
	defer sentry.Flush(2 * time.Second)

	// Запуск миграции данных
	if err = migration.Up(ctx, cfg.DatabaseUrl(), review.EmbedMigrations); err != nil {
		slog.Error("migration up", "error", err)
		return
	}

	pool, err := postgresConnect(ctx, cfg.DatabaseUrl())
	if err != nil {
		slog.Error("creat connect db", "error", err)
		return
	}
	defer pool.Close()

	// Инициализация БД
	s := storage.New(pool)

	//Инициализация gRPC
	grpcAddress := net.JoinHostPort(cfg.App.GRPC.Host, cfg.App.GRPC.Port)
	grpcServer, grpcListen, err := initGRPC(s, grpcAddress, cfg.App.Key)
	if err != nil {
		slog.Error("init gRPC", "error", err)
		return
	}
	defer func() {
		slog.Info("grpc server stopping", "address", grpcListen.Addr())
		grpcServer.GracefulStop()
	}()

	// Инициализация HTTP сервера
	httpAddress := net.JoinHostPort(cfg.App.HTTP.Host, cfg.App.HTTP.Port)
	httpServer, cancel, err := initHTTP(ctx, grpcAddress, httpAddress)
	if err != nil {
		slog.Error("init http", "error", err)
		return
	}
	defer func() {
		slog.Info("http server stopping", "address", httpAddress)
		cancel()
	}()

	// Запуск серверов
	go func() {
		slog.Info("grpc server listening", "address", grpcListen.Addr())
		if err := grpcServer.Serve(grpcListen); err != nil {
			slog.Error("failed start grpc server")
			panic("failed start grpc server")
		}
	}()

	go func() {
		slog.Info("http server listening", "address", httpServer.Addr)
		if err := httpServer.ListenAndServe(); err != nil {
			slog.Error("failed start http server")
			panic(err)
		}
	}()

	stop := <-signalListener
	slog.Info("start shutdown because", "event", stop.String())
}

// Подключение к postgreSQL
func postgresConnect(ctx context.Context, url string) (*pgxpool.Pool, error) {

	cfg, err := pgxpool.ParseConfig(url)
	if err != nil {
		return nil, err
	}

	cfg.MaxConns = _maxOpenConn
	cfg.MaxConnIdleTime = _maxIdleConn
	cfg.MaxConnLifetime = _maxConnLifetime

	pool, err := pgxpool.NewWithConfig(ctx, cfg)
	if err != nil {
		return nil, err
	}

	if err := pool.Ping(ctx); err != nil {
		return nil, err
	}

	return pool, nil
}

func initGRPC(s *storage.Storage, address, key string) (*grpc.Server, net.Listener, error) {
	reviewService := reviewv1.New(s)

	// Инициализация gRPC сервера
	grpcListen, err := net.Listen("tcp", address)
	if err != nil {
		return nil, nil, err
	}

	grpcServer := grpc.NewServer(
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				interceptor.SentryUnaryServerInterceptor(),
				interceptor.AuthUnaryServerInterceptor(key),
			),
		),
	)

	reviewv1pb.RegisterReviewServiceServer(grpcServer, reviewService)

	return grpcServer, grpcListen, nil
}

func initHTTP(ctx context.Context, grpcAddress, httpAddress string) (*http.Server, context.CancelFunc, error) {
	ctx, cancel := context.WithCancel(ctx)

	mux := runtime.NewServeMux(
		// FYI: WithErrorHandler перевод grpc кода ошибки в http код
		runtime.WithErrorHandler(handler.RoutingErrorHandle),
		// FYI: WithMarshalerOption убирает из json ответа пустые поля (подробнее в документации)
		runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{
			MarshalOptions: protojson.MarshalOptions{
				// FYI: возвращает пустые значения json полей
				EmitUnpopulated: true,
			},
			UnmarshalOptions: protojson.UnmarshalOptions{
				// FYI: Запрещает передавать поля, которых нет в API
				DiscardUnknown: false,
			},
		}),
	)

	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	}

	if err := reviewv1pb.RegisterReviewServiceHandlerFromEndpoint(ctx, mux, grpcAddress, opts); err != nil {
		return nil, cancel, err
	}

	httpServer := &http.Server{
		Addr:    httpAddress,
		Handler: mux,
	}

	return httpServer, cancel, nil
}
