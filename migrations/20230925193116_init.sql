-- +goose Up
-- +goose StatementBegin
SELECT 'up SQL query';

CREATE TABLE IF NOT EXISTS  careers(
                                       id SERIAL NOT NULL,
                                       survey_id integer,
                                       self_grade text,
                                       self_planned_role text,
                                       self_comment text,
                                       manager_grade text,
                                       manager_planned_role text,
                                       manager_comment text,
                                       created_at timestamp without time zone DEFAULT now(),
                                       updated_at timestamp without time zone DEFAULT now(),
                                       deleted_at timestamp without time zone,
                                       PRIMARY KEY(id)
);
CREATE UNIQUE INDEX idp_cariers_id_uindex ON "careers" USING btree ("id");
CREATE UNIQUE INDEX idp_cariers_survey_id_index ON "careers" USING btree ("survey_id");

CREATE TABLE IF NOT EXISTS report_notification(
                                                  dataset_id text NOT NULL,
                                                  "date" timestamp without time zone,
                                                  review_id integer NOT NULL,
                                                  created_at timestamp without time zone DEFAULT now(),
                                                  updated_at timestamp without time zone DEFAULT now(),
                                                  deleted_at timestamp without time zone
);
COMMENT ON TABLE "report_notification" IS 'contains notifications data with last sending date';

CREATE TABLE IF NOT EXISTS review360(
                                        id SERIAL NOT NULL,
                                        survey_id integer,
                                        question_id integer,
                                        "value" text,
                                        created_at timestamp without time zone DEFAULT now(),
                                        updated_at timestamp without time zone DEFAULT now(),
                                        deleted_at timestamp without time zone,
                                        PRIMARY KEY(id)
);
CREATE UNIQUE INDEX review360_id_uindex ON "review360" USING btree ("id");
CREATE UNIQUE INDEX review360_survey_id_question_id_uindex ON "review360" USING btree ("survey_id","question_id");

CREATE TABLE IF NOT EXISTS review_delegate_employees(
                                                        created_at timestamp without time zone DEFAULT now(),
                                                        updated_at timestamp without time zone DEFAULT now(),
                                                        review_id integer NOT NULL,
                                                        manager_id uuid NOT NULL,
                                                        delegate_id uuid NOT NULL,
                                                        employee_id uuid NOT NULL,
                                                        status_id integer NOT NULL,
                                                        comment text,
                                                        id SERIAL NOT NULL,
                                                        deleted_at timestamp without time zone
);

CREATE TABLE IF NOT EXISTS review_delegate_employees_history(
                                                                review_id integer NOT NULL,
                                                                manager_id uuid NOT NULL,
                                                                delegate_id uuid NOT NULL,
                                                                employee_id uuid NOT NULL,
                                                                status_id integer NOT NULL,
                                                                comment text,
                                                                created_at timestamp without time zone DEFAULT now(),
                                                                updated_at timestamp without time zone DEFAULT now(),
                                                                deleted_at timestamp without time zone
);

CREATE TABLE IF NOT EXISTS review_employee_respondets(
                                                         reviewed_person_id uuid NOT NULL,
                                                         respondent_id uuid,
                                                         status_id integer,
                                                         review_id integer,
                                                         created_at timestamp without time zone DEFAULT now(),
                                                         updated_at timestamp without time zone DEFAULT now(),
                                                         deleted_at timestamp without time zone
);
CREATE INDEX respondent_list_review_id_index ON "review_employee_respondets" USING btree ("review_id");
CREATE UNIQUE INDEX respondent_list_reviewed_person_id_respondent_id_review_id_uind ON "review_employee_respondets" USING btree ("reviewed_person_id","respondent_id","review_id");

CREATE TABLE IF NOT EXISTS review_employees(
                                               manager_id uuid,
                                               employee_id uuid,
                                               status_id integer,
                                               review_id integer,
                                               created_at timestamp without time zone DEFAULT now(),
                                               updated_at timestamp without time zone DEFAULT now(),
                                               deleted_at timestamp without time zone
);
CREATE INDEX pr_state_review_id_index ON "review_employees" USING btree ("review_id");
CREATE UNIQUE INDEX review_manager_employee_index ON "review_employees" USING btree ("manager_id","employee_id","review_id");

CREATE TABLE IF NOT EXISTS review_skillsets(
                                               employee_id uuid,
                                               expert_id uuid,
                                               status_id integer,
                                               review_id integer,
                                               created_at timestamp without time zone DEFAULT now(),
                                               updated_at timestamp without time zone DEFAULT now(),
                                               deleted_at timestamp without time zone,
                                               is_skillset_on boolean DEFAULT false,
                                               skillset_id integer
);
CREATE INDEX review_skillsets_expert_id_index ON "review_skillsets" USING btree ("expert_id");
CREATE UNIQUE INDEX review_skillsets_review_id_employee_id_expert_id_index ON "review_skillsets" USING btree ("review_id", "employee_id", "expert_id");

CREATE TABLE IF NOT EXISTS reviews(
                                      id SERIAL NOT NULL,
                                      review_start date,
                                      process_start date,
                                      approval_start date,
                                      review_end date,
                                      is_canceled boolean NOT NULL DEFAULT false,
                                      canceled_date date,
                                      name text,
                                      setup_date date,
                                      summary_date date,
                                      review_type integer DEFAULT 0,
                                      created_at timestamp without time zone DEFAULT now(),
                                      updated_at timestamp without time zone DEFAULT now(),
                                      deleted_at timestamp without time zone,
                                      PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS review_survey_types(
                                                  survey_types_id integer,
                                                  review_id integer,
                                                  created_at timestamp without time zone DEFAULT now(),
                                                  updated_at timestamp without time zone DEFAULT now(),
                                                  deleted_at timestamp without time zone
);
CREATE INDEX review_survey_types_review_id_index ON "review_survey_types" USING btree ("review_id");

CREATE TABLE IF NOT EXISTS selfreview_grade(
                                               id integer NOT NULL,
                                               name text,
                                               "value" integer,
                                               created_at timestamp without time zone DEFAULT now(),
                                               updated_at timestamp without time zone DEFAULT now(),
                                               deleted_at timestamp without time zone,
                                               PRIMARY KEY(id)
);
CREATE UNIQUE INDEX slefreview_grade_id_uindex ON "selfreview_grade" USING btree ("id");

CREATE TABLE IF NOT EXISTS selfreview_items(
                                               id SERIAL NOT NULL,
                                               target_text text,
                                               employee_grade_id integer,
                                               manager_comment text,
                                               manager_grade_id integer,
                                               root_id integer,
                                               created_at timestamp without time zone DEFAULT now(),
                                               updated_at timestamp without time zone DEFAULT now(),
                                               deleted_at timestamp without time zone,
                                               PRIMARY KEY(id)
);
CREATE UNIQUE INDEX selfreview_items_id_uindex ON "selfreview_items" USING btree ("id");
CREATE INDEX selfreview_items_root_id_index ON "selfreview_items" USING btree ("root_id");

CREATE TABLE IF NOT EXISTS selfreviews(
                                          id SERIAL NOT NULL,
                                          survey_id integer,
                                          total_grade_id integer,
                                          created_at timestamp without time zone DEFAULT now(),
                                          updated_at timestamp without time zone DEFAULT now(),
                                          deleted_at timestamp without time zone,
                                          PRIMARY KEY(id)
);
CREATE UNIQUE INDEX selfreviews_id_uindex ON "selfreviews" USING btree ("id");
CREATE INDEX selfreviews_survey_id_index ON "selfreviews" USING btree ("survey_id");


CREATE TABLE IF NOT EXISTS surveys(
                                      id SERIAL NOT NULL,
                                      type_id integer,
                                      status_id integer,
                                      review_id integer,
                                      employee_id uuid,
                                      manager_id uuid,
                                      expert_id uuid,
                                      respondent_id uuid,
                                      created_at timestamp without time zone DEFAULT now(),
                                      updated_at timestamp without time zone DEFAULT now(),
                                      deleted_at timestamp without time zone DEFAULT now(),
                                      revision_comment text,
                                      PRIMARY KEY(id)
);
CREATE UNIQUE INDEX surveys_type_id_employee_id_manager_id_respondent_id_expert_id_ ON "surveys" USING btree ("type_id","review_id","employee_id","manager_id","expert_id","respondent_id");

CREATE TABLE IF NOT EXISTS survey_feedback(
                                              id SERIAL NOT NULL,
                                              survey_id integer NOT NULL,
                                              survey_type integer NOT NULL,
                                              comment text,
                                              created_at timestamp without time zone NOT NULL DEFAULT now(),
                                              updated_at timestamp without time zone NOT NULL DEFAULT now(),
                                              deleted_at timestamp without time zone,
                                              skillset_id integer,
                                              user_email text NOT NULL,
                                              user_id uuid NOT NULL,
                                              PRIMARY KEY(id),
                                              CONSTRAINT survey_feedback_surveys_id_fk FOREIGN key(survey_id) REFERENCES surveys(id)
);

CREATE TABLE IF NOT EXISTS survey_skillset_answers(
                                                      id SERIAL NOT NULL,
                                                      survey_id bigint,
                                                      answer_id bigint,
                                                      created_at timestamp without time zone NOT NULL DEFAULT now(),
                                                      updated_at timestamp without time zone NOT NULL DEFAULT now(),
                                                      deleted_at time without time zone,
                                                      question_id bigint,
                                                      PRIMARY KEY(id)
);
CREATE UNIQUE INDEX survey_skillsets_survey_id_question_id_uindex ON "survey_skillset_answers" USING btree ("survey_id","question_id");
CREATE INDEX surveys_skillsets_survey_id_index ON "survey_skillset_answers" USING btree ("survey_id");

CREATE TABLE IF NOT EXISTS survey_types(
                                           id integer NOT NULL,
                                           name character varying(25),
                                           description text,
                                           created_at timestamp without time zone DEFAULT now(),
                                           updated_at timestamp without time zone DEFAULT now(),
                                           deleted_at timestamp without time zone,
                                           PRIMARY KEY(id)
);
COMMENT ON TABLE "survey_types" IS 'Types of surveys';

CREATE TABLE IF NOT EXISTS survey_statuses(
                                              id integer NOT NULL,
                                              name character varying(25),
                                              description text,
                                              indicator_id integer,
                                              created_at timestamp without time zone NOT NULL DEFAULT now(),
                                              updated_at timestamp without time zone NOT NULL DEFAULT now(),
                                              deleted_at timestamp without time zone,
                                              PRIMARY KEY(id)
);

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
SELECT 'down SQL query';

DROP TABLE IF EXISTS survey_statuses;
DROP TABLE IF EXISTS careers;
DROP TABLE IF EXISTS report_notification;
DROP TABLE IF EXISTS review360;
DROP TABLE IF EXISTS review_delegate_employees;
DROP TABLE IF EXISTS review_delegate_employees_history;
DROP TABLE IF EXISTS review_employee_respondets;
DROP TABLE IF EXISTS review_employees;
DROP TABLE IF EXISTS review_skillsets;
DROP TABLE IF EXISTS review_survey_types;
DROP TABLE IF EXISTS selfreview_grade;
DROP TABLE IF EXISTS selfreview_items;
DROP TABLE IF EXISTS selfreviews;
DROP TABLE IF EXISTS survey_skillset_answers;
DROP TABLE IF EXISTS survey_feedback;
DROP TABLE IF EXISTS surveys;
DROP TABLE IF EXISTS survey_types;
DROP TABLE IF EXISTS reviews;

-- +goose StatementEnd
