package storage

import (
	"context"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"github.com/georgysavva/scany/v2/pgxscan"
	"github.com/go-errors/errors"
	"gitlab.services.mts.ru/performance-review/review/internal/dto"
	"time"
)

func (s *Storage) CreateReview(ctx context.Context, review *dto.Review) (*dto.Review, error) {
	const fn = "internal.storage.review.CreateReview"

	tx, err := s.pool.Begin(ctx)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}

	defer func() {
		if err != nil {
			_ = tx.Rollback(ctx)
			return
		}
	}()

	req, args, err := sq.
		Insert("reviews").
		Columns(
			"name",
			"review_start",
			"setup_date",
			"process_start",
			"approval_start",
			"summary_date",
			"review_end",
		).
		Values(
			review.Name,
			review.ReviewStart,
			review.SetupDate,
			review.ProcessStart,
			review.ApprovalStart,
			review.SummaryDate,
			review.ReviewEnd,
		).
		Suffix("RETURNING id").
		PlaceholderFormat(sq.Dollar).
		ToSql()

	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}

	rowReviewId, err := tx.Query(ctx, req, args...)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}
	defer rowReviewId.Close()

	reviewId := 0
	if err := pgxscan.ScanOne(&reviewId, rowReviewId); err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}

	sb := sq.StatementBuilder.
		Insert("review_survey_types").
		Columns(
			"review_id",
			"survey_types_id",
		).
		PlaceholderFormat(sq.Dollar)

	for _, survey := range review.Surveys {
		sb = sb.Values(
			reviewId,
			survey.ID,
		)
	}

	req, args, err = sb.ToSql()

	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}

	_, err = tx.Exec(ctx, req, args...)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}

	err = tx.Commit(ctx)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}

	response, err := s.GetReviewWithSurveysTypes(ctx, reviewId)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}

	return response, nil
}

func (s *Storage) GetReviewWithSurveysTypes(ctx context.Context, reviewId int) (*dto.Review, error) {
	const fn = "internal.storage.review.GetReviewWithSurveysTypes"

	req, args, err := sq.
		Select(
			"r.id",
			"r.name as review_name",
			"r.review_start",
			"r.setup_date",
			"r.process_start",
			"r.approval_start",
			"r.summary_date",
			"r.review_end",
			"r.is_canceled",
			"rst.survey_types_id",
			"st.name as survey_name",
			"st.description",
		).
		From("reviews r").
		LeftJoin("review_survey_types rst on rst.review_id = r.id and rst.deleted_at is null").
		LeftJoin("survey_types st on st.id = rst.survey_types_id and st.deleted_at is null").
		Where(
			sq.And{
				sq.Eq{
					"r.deleted_at": nil,
				},
				sq.Eq{
					"r.id": reviewId,
				},
			},
		).
		OrderBy("id").
		PlaceholderFormat(sq.Dollar).
		ToSql()

	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}

	rows, err := s.pool.Query(ctx, req, args...)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}
	defer rows.Close()

	review := make([]Review, 0, len(rows.RawValues()))
	if err := pgxscan.ScanAll(&review, rows); err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}

	response := &dto.Review{
		ID:   review[0].ID,
		Name: review[0].ReviewName.String,
	}

	if review[0].ReviewStart.Valid {
		response.ReviewStart = &review[0].ReviewStart.Time
	}

	if review[0].SetupDate.Valid {
		response.SetupDate = &review[0].SetupDate.Time
	}

	if review[0].ProcessStart.Valid {
		response.ProcessStart = &review[0].ProcessStart.Time
	}

	if review[0].ApprovalStart.Valid {
		response.ApprovalStart = &review[0].ApprovalStart.Time
	}

	if review[0].SummaryDate.Valid {
		response.SummaryDate = &review[0].SummaryDate.Time
	}

	if review[0].ReviewEnd.Valid {
		response.ReviewEnd = &review[0].ReviewEnd.Time
	}

	response.Surveys = make([]dto.Survey, 0, len(review))

	for _, survey := range review {
		addSurvey := dto.Survey{
			ID:          survey.SurveyTypesId.String,
			Name:        survey.SurveyName.String,
			Description: survey.SurveyDescription.String,
		}
		response.Surveys = append(response.Surveys, addSurvey)
	}

	reviewStatus, err := getReviewStatus(
		response.ReviewStart,
		response.SetupDate,
		response.ProcessStart,
		response.ApprovalStart,
		response.SummaryDate,
		response.ReviewEnd,
		review[0].IsCanceled.Bool,
	)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err.Error())
	}

	response.Status = *reviewStatus

	return response, nil
}

func (s *Storage) GetAllReviewsWithSurveysTypes(ctx context.Context) ([]*dto.Review, error) {
	const fn = "internal.storage.reviewsData.GetAllReviewsWithSurveysTypes"

	req, args, err := sq.
		Select(
			"r.id",
			"r.name as review_name",
			"r.review_start",
			"r.setup_date",
			"r.process_start",
			"r.approval_start",
			"r.summary_date",
			"r.review_end",
			"r.is_canceled",
			"rst.survey_types_id",
			"st.name as survey_name",
			"st.description",
		).
		From("reviews r").
		LeftJoin("review_survey_types rst on rst.review_id = r.id and rst.deleted_at is null").
		LeftJoin("survey_types st on st.id = rst.survey_types_id and st.deleted_at is null").
		Where(
			sq.Eq{
				"r.deleted_at": nil,
			},
		).
		OrderBy("id").
		PlaceholderFormat(sq.Dollar).
		ToSql()

	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}

	rows, err := s.pool.Query(ctx, req, args...)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}
	defer rows.Close()

	reviewsData := make([]Review, 0, len(rows.RawValues()))
	if err := pgxscan.ScanAll(&reviewsData, rows); err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}

	response, err := convertRawReviewDataToDTOObjectsArray(reviewsData)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err.Error())
	}

	return response, nil
}

func getReviewStatus(
	reviewStart *time.Time,
	setupDate *time.Time,
	processStart *time.Time,
	approvalStart *time.Time,
	summaryDate *time.Time,
	reviewEnd *time.Time,
	isCanceled bool,
) (*dto.ReviewStatus, error) {

	currentTime := time.Now()

	if isCanceled == true {
		return REVIEW_STATUS_ARCHIVE(), nil
	}

	if reviewEnd != nil {
		if currentTime.After(*reviewEnd) {
			return REVIEW_STATUS_ARCHIVE(), nil
		}
	}

	if reviewStart != nil {
		if currentTime.Before(*reviewStart) {
			return REVIEW_STATUS_SOONSTART(), nil
		}
	}

	if reviewStart != nil && setupDate != nil {
		if currentTime.After(*reviewStart) && currentTime.Before(*setupDate) {
			return REVIEW_STATUS_RESPONDENT_SELECT(), nil
		}
	}

	if reviewStart == nil && setupDate != nil {
		if currentTime.Before(*setupDate) {
			return REVIEW_STATUS_SOONSTART(), nil
		}
	}

	if setupDate != nil && processStart != nil {
		if currentTime.After(*setupDate) && currentTime.Before(*processStart) {
			return REVIEW_STATUS_SETUP(), nil
		}
	}

	if processStart != nil && approvalStart != nil {
		if currentTime.After(*processStart) && currentTime.Before(*approvalStart) {
			return REVIEW_STATUS_PROCESS(), nil
		}
	}

	if approvalStart != nil && summaryDate != nil {
		if currentTime.After(*approvalStart) && currentTime.Before(*summaryDate) {
			return REVIEW_STATUS_APPROVAL(), nil
		}
	}

	if summaryDate != nil && reviewEnd != nil {
		if currentTime.After(*summaryDate) && currentTime.Before(*reviewEnd) {
			return REVIEW_STATUS_SUMMARY(), nil
		}
	}

	return nil, errors.New("wrong review stages times sequences")

}

func convertRawReviewDataToDTOObjectsArray(source []Review) ([]*dto.Review, error) {
	const fn = "internal.storage.reviewsData.GetAllReviewsWithSurveysTypes"
	response := make([]*dto.Review, 0, len(source))

	currentReviewID := ""
	currentReview := (*dto.Review)(nil)
	cycleStart := true

	for _, reviewRow := range source {

		if reviewRow.ID != currentReviewID {

			if !cycleStart {
				response = append(response, currentReview)
			}
			cycleStart = false

			currentReview = &dto.Review{
				ID:   reviewRow.ID,
				Name: reviewRow.ReviewName.String,
			}

			if reviewRow.ReviewStart.Valid {
				currentReview.ReviewStart = &reviewRow.ReviewStart.Time
			}

			if reviewRow.SetupDate.Valid {
				currentReview.SetupDate = &reviewRow.SetupDate.Time
			}

			if reviewRow.ProcessStart.Valid {
				currentReview.ProcessStart = &reviewRow.ProcessStart.Time
			}

			if reviewRow.ApprovalStart.Valid {
				currentReview.ApprovalStart = &reviewRow.ApprovalStart.Time
			}

			if reviewRow.SummaryDate.Valid {
				currentReview.SummaryDate = &reviewRow.SummaryDate.Time
			}

			if reviewRow.ReviewEnd.Valid {
				currentReview.ReviewEnd = &reviewRow.ReviewEnd.Time
			}

			reviewStatus, err := getReviewStatus(
				currentReview.ReviewStart,
				currentReview.SetupDate,
				currentReview.ProcessStart,
				currentReview.ApprovalStart,
				currentReview.SummaryDate,
				currentReview.ReviewEnd,
				reviewRow.IsCanceled.Bool,
			)

			if err != nil {
				return nil, fmt.Errorf("%s: %w", fn, err.Error())
			}

			currentReview.Status = *reviewStatus

			currentReview.Surveys = make([]dto.Survey, 0, _MaxSurveyTypes)

			currentReviewID = reviewRow.ID
		}

		addSurvey := dto.Survey{
			ID:          reviewRow.SurveyTypesId.String,
			Name:        reviewRow.SurveyName.String,
			Description: reviewRow.SurveyDescription.String,
		}
		currentReview.Surveys = append(currentReview.Surveys, addSurvey)

	}

	response = append(response, currentReview)

	return response, nil
}
