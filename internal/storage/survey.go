package storage

import (
	"context"
	"fmt"

	"gitlab.services.mts.ru/performance-review/review/internal/dto"

	sq "github.com/Masterminds/squirrel"
	"github.com/georgysavva/scany/v2/pgxscan"
)

func (s *Storage) GetSurveysTypes(ctx context.Context) ([]dto.Survey, error) {
	const fn = "internal.storage.surveys.GetSurveysTypes"

	req, args, err := sq.
		Select(
			"id",
			"name",
			"description",
		).
		From("survey_types").
		Where(
			sq.Eq{
				"deleted_at": nil,
			},
		).
		OrderBy("id").
		PlaceholderFormat(sq.Dollar).
		ToSql()

	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}

	rows, err := s.pool.Query(ctx, req, args...)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}
	defer rows.Close()

	survs := make([]Survey, 0, len(rows.RawValues()))
	if err := pgxscan.ScanAll(&survs, rows); err != nil {
		return nil, fmt.Errorf("%s: %w", fn, err)
	}

	response := make([]dto.Survey, 0, len(survs))
	for _, survey := range survs {
		addDTOSurvey := dto.Survey{
			ID:          survey.ID,
			Name:        survey.Name.String,
			Description: survey.Description.String,
		}
		response = append(response, addDTOSurvey)
	}

	return response, nil
}
