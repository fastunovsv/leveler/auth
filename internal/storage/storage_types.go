package storage

import (
	"database/sql"
	"gitlab.services.mts.ru/performance-review/review/internal/dto"
)

const (
	_MaxSurveyTypes = 6
)

type Survey struct {
	ID          string         `db:"id"`
	Name        sql.NullString `db:"name"`
	Description sql.NullString `db:"description"`
}

type Review struct {
	ID                string         `db:"id"`
	ReviewName        sql.NullString `db:"review_name"`
	ReviewStart       sql.NullTime   `db:"review_start"`
	SetupDate         sql.NullTime   `db:"setup_date"`
	ProcessStart      sql.NullTime   `db:"process_start"`
	ApprovalStart     sql.NullTime   `db:"approval_start"`
	SummaryDate       sql.NullTime   `db:"summary_date"`
	ReviewEnd         sql.NullTime   `db:"review_end"`
	IsCanceled        sql.NullBool   `db:"is_canceled"`
	SurveyTypesId     sql.NullString `db:"survey_types_id"`
	SurveyName        sql.NullString `db:"survey_name"`
	SurveyDescription sql.NullString `db:"description"`
}

func REVIEW_STATUS_ARCHIVE() *dto.ReviewStatus {
	return &dto.ReviewStatus{
		ID:         "1",
		Value:      "Архив",
		StatusType: "4",
	}
}

func REVIEW_STATUS_RESPONDENT_SELECT() *dto.ReviewStatus {
	return &dto.ReviewStatus{
		ID:         "8",
		Value:      "Этап выбора респондентов",
		StatusType: "3",
	}
}

func REVIEW_STATUS_SOONSTART() *dto.ReviewStatus {
	return &dto.ReviewStatus{
		ID:         "2",
		Value:      "Скоро начало",
		StatusType: "3",
	}
}

func REVIEW_STATUS_SETUP() *dto.ReviewStatus {
	return &dto.ReviewStatus{
		ID:         "3",
		Value:      "Этап настройки",
		StatusType: "3",
	}
}

func REVIEW_STATUS_PROCESS() *dto.ReviewStatus {
	return &dto.ReviewStatus{
		ID:         "4",
		Value:      "Этап прохождения",
		StatusType: "3",
	}
}

func REVIEW_STATUS_SUMMARY() *dto.ReviewStatus {
	return &dto.ReviewStatus{
		ID:         "6",
		Value:      "Подведение итогов",
		StatusType: "3",
	}
}

func REVIEW_STATUS_APPROVAL() *dto.ReviewStatus {
	return &dto.ReviewStatus{
		ID:         "5",
		Value:      "Этап согласования",
		StatusType: "3",
	}
}
