package reviewv1

import (
	"context"
	"gitlab.services.mts.ru/performance-review/go-lib/grpc/status"
	"gitlab.services.mts.ru/performance-review/review/internal/lib/auth"
	"gitlab.services.mts.ru/performance-review/review/internal/lib/converters"
	grpcerr "gitlab.services.mts.ru/performance-review/review/internal/lib/grpc/errors"
	typespb "gitlab.services.mts.ru/performance-review/talent-review-api/gen/go/types"
	"google.golang.org/grpc/codes"

	reviewv1pb "gitlab.services.mts.ru/performance-review/talent-review-api/gen/go/review/v1"
)

func (rv *ReviewServiceV1) GetAdminReviews(ctx context.Context, _ *reviewv1pb.GetAdminReviewsRequest) (*reviewv1pb.GetAdminReviewsResponse, error) {
	ok, err := auth.CheckAdminClaimsInContext(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if !ok {
		return nil, status.Error(codes.PermissionDenied, grpcerr.ErrPermissionDenied)
	}

	reviewList, err := rv.s.GetAllReviewsWithSurveysTypes(ctx)
	if err != nil {
		return nil, err
	}

	response := &reviewv1pb.GetAdminReviewsResponse{
		Reviews: make([]*typespb.AdminReiew, 0, len(reviewList)),
	}

	for _, review := range reviewList {
		addReviewItem := converters.ReviewDTOToAdminReviewAPI(review)
		response.Reviews = append(response.Reviews, addReviewItem)
	}

	return response, nil
}
