package reviewv1

import (
	"context"
	"gitlab.services.mts.ru/performance-review/review/internal/dto"
	reviewv1pb "gitlab.services.mts.ru/performance-review/talent-review-api/gen/go/review/v1"
)

type ReviewServiceV1 struct {
	s DAO

	reviewv1pb.UnimplementedReviewServiceServer
}

func New(s DAO) *ReviewServiceV1 {
	return &ReviewServiceV1{
		s: s,
	}
}

type DAO interface {
	GetSurveysTypes(ctx context.Context) ([]dto.Survey, error)
	CreateReview(ctx context.Context, review *dto.Review) (*dto.Review, error)
	GetReviewWithSurveysTypes(ctx context.Context, reviewId int) (*dto.Review, error)
	GetAllReviewsWithSurveysTypes(ctx context.Context) ([]*dto.Review, error)
}
