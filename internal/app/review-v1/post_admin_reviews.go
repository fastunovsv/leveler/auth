package reviewv1

import (
	"context"
	"gitlab.services.mts.ru/performance-review/review/internal/lib/converters"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"strings"
	"time"

	"gitlab.services.mts.ru/performance-review/go-lib/grpc/status"
	reviewv1pb "gitlab.services.mts.ru/performance-review/talent-review-api/gen/go/review/v1"
	apitypes "gitlab.services.mts.ru/performance-review/talent-review-api/gen/go/types"

	"gitlab.services.mts.ru/performance-review/review/internal/lib/auth"
	grpcerr "gitlab.services.mts.ru/performance-review/review/internal/lib/grpc/errors"
)

func (rv *ReviewServiceV1) PostAdminReviews(ctx context.Context, in *reviewv1pb.PostAdminReviewsRequest) (*apitypes.AdminReiew, error) {
	ok, err := auth.CheckAdminClaimsInContext(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if !ok {
		return nil, status.Error(codes.PermissionDenied, grpcerr.ErrPermissionDenied)
	}

	if details, ok := validatePostAdminReviews(in); !ok {
		return nil, status.ErrorWithDetails(codes.InvalidArgument, grpcerr.ErrInvalidArgument, details)
	}

	review := converters.PostAdminReviewAPIToReviewDTO(in)

	result, err := rv.s.CreateReview(ctx, review)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return converters.ReviewDTOToAdminReviewAPI(result), nil
}

func validatePostAdminReviews(in *reviewv1pb.PostAdminReviewsRequest) (status.Details, bool) {

	if strings.TrimSpace(in.Name) == "" {
		return status.Details{"invalid Name"}, false
	}

	// NOTE: Порядок следования переменных в аргументах важен
	ok, errStatus := checkReviewStagesTimes(
		in.ReviewStart,
		in.SetupDate,
		in.ProcessStart,
		in.ApprovalStart,
		in.SummaryDate,
		in.ReviewEnd,
	)

	if !ok {
		return errStatus, false
	}

	if len(in.SurveyIdList) == 0 {
		return status.Details{"zero Survey types numbers"}, false
	}

	return nil, true
}

// NOTE: Аругменты функции соспоставляются в порядке указания. Важно соблюдать правильную последовательность этапов
func checkReviewStagesTimes(ReviewSatagesTimes ...*timestamppb.Timestamp) (bool, status.Details) {
	checkDate := time.Now()

	for _, rst := range ReviewSatagesTimes {
		if rst != nil {
			if !rst.IsValid() {
				return false, status.Details{"invalid time format"}
			}

			if rst.AsTime().Before(checkDate) {
				return false, status.Details{"wrong review stages times sequence"}
			}

			checkDate = rst.AsTime()
		}
	}

	return true, nil
}
