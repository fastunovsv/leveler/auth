package reviewv1

import (
	"context"
	"gitlab.services.mts.ru/performance-review/review/internal/lib/auth"
	grpcerr "gitlab.services.mts.ru/performance-review/review/internal/lib/grpc/errors"

	"gitlab.services.mts.ru/performance-review/go-lib/grpc/status"
	reviewv1pb "gitlab.services.mts.ru/performance-review/talent-review-api/gen/go/review/v1"
	typespb "gitlab.services.mts.ru/performance-review/talent-review-api/gen/go/types"

	"google.golang.org/grpc/codes"
)

func (rv *ReviewServiceV1) GetAdminSurveys(ctx context.Context, _ *reviewv1pb.GetAdminSurveysRequest) (*reviewv1pb.GetAdminSurveysResponse, error) {

	ok, err := auth.CheckAdminClaimsInContext(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if !ok {
		return nil, status.Error(codes.PermissionDenied, grpcerr.ErrPermissionDenied)
	}

	s, err := rv.s.GetSurveysTypes(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	response := make([]*typespb.Survey, 0, len(s))

	for _, surv := range s {
		responseElement := &typespb.Survey{
			Id:   surv.ID,
			Name: surv.Description,
		}
		response = append(response, responseElement)
	}

	return &reviewv1pb.GetAdminSurveysResponse{
		Surveys: response,
	}, nil
}
