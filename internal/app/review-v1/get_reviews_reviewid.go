package reviewv1

import (
	"context"
	"gitlab.services.mts.ru/performance-review/review/internal/lib/converters"
	"strconv"

	"gitlab.services.mts.ru/performance-review/go-lib/grpc/status"
	reviewv1pb "gitlab.services.mts.ru/performance-review/talent-review-api/gen/go/review/v1"
	apitypes "gitlab.services.mts.ru/performance-review/talent-review-api/gen/go/types"

	"google.golang.org/grpc/codes"
)

func (rv *ReviewServiceV1) GetAdminReviewById(ctx context.Context, in *reviewv1pb.GetAdminReviewByIdRequest) (*apitypes.AdminReiew, error) {

	reviewId, err := strconv.Atoi(in.GetReviewId())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	result, err := rv.s.GetReviewWithSurveysTypes(ctx, reviewId)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return converters.ReviewDTOToAdminReviewAPI(result), nil
}
