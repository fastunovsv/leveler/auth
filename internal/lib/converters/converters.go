package converters

import (
	"strconv"
	"time"

	"gitlab.services.mts.ru/performance-review/review/internal/dto"

	reviewv1pb "gitlab.services.mts.ru/performance-review/talent-review-api/gen/go/review/v1"
	apitypes "gitlab.services.mts.ru/performance-review/talent-review-api/gen/go/types"

	"google.golang.org/protobuf/types/known/timestamppb"
)

func ReviewDTOToAdminReviewAPI(review *dto.Review) *apitypes.AdminReiew {
	response := apitypes.AdminReiew{
		Id:      review.ID,
		Name:    review.Name,
		Surveys: make([]*apitypes.Survey, 0, len(review.Surveys)),
		Status: &apitypes.ReviewStatus{
			Id:         review.Status.ID,
			Value:      review.Status.Value,
			StatusType: review.Status.StatusType,
		},
	}

	if review.ReviewStart != nil {
		response.ReviewStart = timestamppb.New(*review.ReviewStart)
	}

	if review.SetupDate != nil {
		response.SetupDate = timestamppb.New(*review.SetupDate)
	}

	if review.ProcessStart != nil {
		response.ProcessStart = timestamppb.New(*review.ProcessStart)
	}

	if review.ApprovalStart != nil {
		response.ApprovalStart = timestamppb.New(*review.ApprovalStart)
	}

	if review.SummaryDate != nil {
		response.SummaryDate = timestamppb.New(*review.SummaryDate)
	}

	if review.ReviewEnd != nil {
		response.ReviewEnd = timestamppb.New(*review.ReviewEnd)
	}

	for _, survey := range review.Surveys {
		addItem := &apitypes.Survey{
			Id:   survey.ID,
			Name: survey.Description,
		}
		response.Surveys = append(response.Surveys, addItem)
	}

	return &response
}

func PostAdminReviewAPIToReviewDTO(review *reviewv1pb.PostAdminReviewsRequest) *dto.Review {
	var (
		reviewStart   *time.Time
		setupDate     *time.Time
		processStart  *time.Time
		approvalStart *time.Time
		summaryDate   *time.Time
		reviewEnd     *time.Time
	)

	if review.ReviewStart != nil {
		t := review.ReviewStart.AsTime()
		reviewStart = &t
	}

	if review.SetupDate != nil {
		t := review.SetupDate.AsTime()
		setupDate = &t
	}

	if review.ProcessStart != nil {
		t := review.ProcessStart.AsTime()
		processStart = &t
	}

	if review.ApprovalStart != nil {
		t := review.ApprovalStart.AsTime()
		approvalStart = &t
	}

	if review.SummaryDate != nil {
		t := review.SummaryDate.AsTime()
		summaryDate = &t
	}

	if review.ReviewEnd != nil {
		t := review.ReviewEnd.AsTime()
		reviewEnd = &t
	}

	response := &dto.Review{
		Name:          review.Name,
		ReviewStart:   reviewStart,
		SetupDate:     setupDate,
		ProcessStart:  processStart,
		ApprovalStart: approvalStart,
		SummaryDate:   summaryDate,
		ReviewEnd:     reviewEnd,
		Surveys:       make([]dto.Survey, 0, len(review.SurveyIdList)),
	}

	for _, surveyId := range review.SurveyIdList {
		addSurvey := dto.Survey{
			ID: strconv.Itoa(int(surveyId)),
		}
		response.Surveys = append(response.Surveys, addSurvey)
	}

	return response
}
