package auth

import (
	"context"
	"github.com/golang-jwt/jwt"
	"github.com/samber/lo"
	"gitlab.services.mts.ru/performance-review/go-lib/grpc/status"
	grpcerr "gitlab.services.mts.ru/performance-review/review/internal/lib/grpc/errors"
	"google.golang.org/grpc/codes"
)

const (
	Key = "auth"
)

const (
	IsInAdminTable = "isAdmin"
	IsInRootTable  = "isRoot"
)

const (
	ReportInvolvementRole  = "report_involvement"
	SendNotificationRole   = "send_notification"
	PositionComplianceRole = "position_compliance"
	PersonnelReserveRole   = "personnel_reserve"
	DoUpstairsRole         = "personnel_reserve"
	ReviewSetupRole        = "review_setup"
	TemplateEditRole       = "template_edit"
)

type AuthClaims struct {
	jwt.StandardClaims
	Username      string   `json:"username"`
	OriginalToken string   `json:"originalToken"`
	Access        []string `json:"access"`
	UserId        string   `json:"userId"`
}

func CheckAdminClaimsInContext(ctx context.Context) (bool, error) {
	claims, ok := ctx.Value(Key).(*AuthClaims)
	if !ok {
		return false, status.Errorf(codes.Internal, grpcerr.ErrInternalServer)
	}

	_, ok = lo.Find(
		claims.Access,
		func(i string) bool {
			return (i == IsInAdminTable) || (i == IsInRootTable)
		},
	)

	return ok, nil
}
