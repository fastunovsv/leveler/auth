package interceptor

import (
	"context"
	"strings"

	"github.com/golang-jwt/jwt"
	"gitlab.services.mts.ru/performance-review/review/internal/lib/auth"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

func AuthUnaryServerInterceptor(key string) func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return nil, status.Errorf(codes.InvalidArgument, "Retrieving metadata is failed")
		}

		authHeader, ok := md["authorization"]
		if !ok {
			return nil, status.Errorf(codes.Unauthenticated, "Authorization token is not supplied")
		}

		splitToken := strings.Split(authHeader[0], "Bearer ")
		if len(splitToken) < 2 {
			return nil, status.Errorf(codes.InvalidArgument, "Retrieving metadata is failed")
		}
		tokenStr := splitToken[1]
		token, err := jwt.ParseWithClaims(tokenStr, &auth.AuthClaims{}, func(token *jwt.Token) (interface{}, error) {
			return []byte(key), nil

		})
		if err != nil {
			return nil, status.Errorf(codes.InvalidArgument, "Retrieving metadata is failed")
		}

		claims, ok := token.Claims.(*auth.AuthClaims)

		if !ok || !token.Valid {
			return nil, status.Errorf(codes.InvalidArgument, "Retrieving metadata is failed")
		}

		ctx = context.WithValue(ctx, auth.Key, claims)

		return handler(ctx, req)
	}
}
